\begin{thenomenclature} 

 \nomgroup{A}

  \item [{$\eta$}]\begingroup Device dependent parameter between 1 and 2\nomeqref {5}
		\nompageref{2}
  \item [{$\nu$}]\begingroup Light Frequency\nomeqref {5}\nompageref{2}
  \item [{$\rho$}]\begingroup Friction Index\nomeqref {5}\nompageref{2}
  \item [{$\xi$}]\begingroup Light spatial frequency\nomeqref {5}
		\nompageref{2}
  \item [{$A$}]\begingroup Constant\nomeqref {5}\nompageref{2}
  \item [{$a$}]\begingroup Slope of linear function\nomeqref {5}
		\nompageref{2}
  \item [{$b$}]\begingroup y-axis intercept of linear function\nomeqref {5}
		\nompageref{2}
  \item [{$e$}]\begingroup Electron Charge\nomeqref {5}\nompageref{2}
  \item [{$E_g$}]\begingroup Energy Band Gap\nomeqref {5}\nompageref{2}
  \item [{$h$}]\begingroup Plank Constant\nomeqref {5}\nompageref{2}
  \item [{$I$}]\begingroup Current\nomeqref {5}\nompageref{2}
  \item [{$k_b$}]\begingroup Boltzmann Constant\nomeqref {5}
		\nompageref{2}
  \item [{$T$}]\begingroup Absolute Temperature\nomeqref {5}
		\nompageref{2}
  \item [{$V$}]\begingroup Voltage\nomeqref {5}\nompageref{2}
  \item [{$x_{int}$}]\begingroup x-axis intercept\nomeqref {5}
		\nompageref{2}

\end{thenomenclature}
