import numpy as np
import matplotlib.pyplot as plt

from kapteyn import kmpfit
intensities = np.loadtxt('./intensities_angle.txt', delimiter=",")
angle = np.linspace(0, 340, 18)*np.pi/(180)
def res(p, x):
    epsilon, I0, I_min = p
    return (I0*np.cos(x-epsilon)**2+I_min)


def residuals(p, data):
   # Merit function for data with errors in both coordinates
   a, b, c = p
   x, y = data
   ey = 0.2
   ex = np.pi/180
   w1 = ey*ey + (2*a*np.sin(b-x)*np.cos(b-x))**2*ex*ex
   w = np.sqrt(np.where(w1==0.0, 0.0, 1.0/(w1)))
   d = w*(y-res(p,x))
   return d
paramsinitial = [1.0,1.0,0.1]
fitobj = kmpfit.Fitter(residuals=residuals, data=(angle,intensities))
fitobj.fit(params0=paramsinitial)
print("\nFit status kmpfit:")
print("====================")
print("Best-fit parameters:        ", fitobj.params)
print("Asymptotic error:           ", fitobj.xerror)
print("Error assuming red.chi^2=1: ", fitobj.stderr)
print("Chi^2 min:                  ", fitobj.chi2_min)
print("Reduced Chi^2:              ", fitobj.rchi2_min)
print("Iterations:                 ", fitobj.niter)
print("Number of free pars.:       ", fitobj.nfree)
print("Degrees of freedom:         ", fitobj.dof)
